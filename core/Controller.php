<?php

namespace Core;
use Core\{View,Config};


class Controller
{
   private $__controllerName, $__actionName;
   public $view, $request;

   public function __construct($controller, $action){
     $this->__controllerName = $controller;
     $this->__actionName = $action;
     $viewPath = strtolower($controller) . '/' .$action;
     $this->view = new View($viewPath);
     $this->view->setLayout(Config::get('default_layout'));
   }

}