<?php
namespace Core;
use \PDO;
use \Exception;
use Core\{Config, H};


class DB{
    protected $__dbh;
    protected $__results;
    protected $__lastInserteId;
    protected $__rowcount = 0;
    protected $__fetchType = PDO::FETCH_OBJ;
    protected $__class;
    protected $__error = false;
    protected $__stmt = false;
    protected static $__db;


    public function __construct(){
        $host = Config::get('db_host');
        $name = Config::get('db_name');
        $user = Config::get('db_user');
        $pass = Config::get('db_password');
        $options = [
            PDO::ATTR_EMULATE_PREPARES => false,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        ];

        try {
            //code...
            $this->__dbh = new PDO("mysql:host={$host};dbname={$name}", $user, $pass, $options);
        } catch (Exception $e) {
            //throw $th;
            throw new Exception($e->getMessage());
        }
    }

    public static function getInstance() {
        if (!self::$__db) {
            self::$__db = new self();
        }
        return self::$__db;
    }

    public function execute($sql, $bind = []) {
       $this->__results = null;
       $this->__lastInserteId = null;
       $this->__error = false;
       $this->__stmt = $this->__dbh->prepare($sql);

       if (!$this->__stmt->execute($bind)) {
         $this->__error = true;
       }else{
            $this->__lastInserteId = $this->__dbh->lastInsertId();
       }
       
       return $this;
    }

    #Query function
    public function query($sql, $bind = []){
        $this->execute($sql, $bind);
        if (!$this->__error) {
            $this->__rowcount = $this->__stmt->rowCount();
            $this->__results = $this->__stmt->fetchAll($this->__fetchType);
        }
        return $this;
    }


    #Insertion function of articles
    public function insert($table, $values){
       $fields = [];
       $binds = [];

       foreach ($values as $key => $value) {
            $fields[] = $key;
            $binds[] = ":{$key}";
       }
       $fieldStr = implode('`,`', $fields);
       $bindStr = implode(',', $binds);
       $sql  = "INSERT INTO {$table} (`{$fieldStr}`) VALUES({$bindStr})";
       $this->execute($sql, $values);
       return !$this->__error;
    }


    #Update Article
    public function update($table, $values, $conditions){
        $binds = [];
        $valueStr = "";
        foreach ($values as $field => $value) {
            $valueStr .= ", `{$field}` = :{$field}";
            $binds[$field] = $value;
        }
        $valueStr = ltrim($valueStr, ', ');
        $sql = "UPDATE {$table} SET {$valueStr}" ;
        
        if (!empty($conditions)) {
            $conditionStr = " WHERE";
            
            foreach ($conditions as $field => $value) {
                $conditionStr .= "`{$field}`= :cond{$field} AND  ";
                $binds['cond'.$field] = $value;
            }
            $conditionStr = rtrim($conditionStr, ' AND');
            $sql .= $conditionStr;
        }
        $this->execute($sql, $binds);
        return !$this->__error;
        //H::dnd($binds);
    }



    public function results() {
        return $this->__results;
    }

    public function count() {
        return $this->__rowcount;
    }

    public function lastInsertId() {
        return $this->__lastInserteId;
    }

    public function setClass($class) {
        $this->__class = $class;
    }

    public function setFetchType($type) {
       $this->__fetchType = $type;
    }

}