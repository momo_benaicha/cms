<?php

namespace Core;

class Config
{
  private static $config = [
     'version'            => '0.0.1', 
     'root_dir'           => '/cms/',
     'default_controller' => 'Blog',
     'default_layout'     => 'default',
     'default_site_title' => 'Benaicha',
     'db_host'            => '127.0.0.1',
     'db_name'            => 'php_cms',
     'db_user'            => 'root',
     'db_password'        => '',
     'login_cookies_name' => 'hakdjri2341l8a',
  ]; 


  public static function get($key) {
    return array_key_exists($key, self::$config) ? self::$config[$key] : NULL;
  }
}