<?php

namespace Core;
use \PDO;
use Core\DB;

class Model{
    protected static $table = "";
    protected static $columns = false;
    protected $__validationPassed = true, $__errors = [], $__skipUpdate = [];

    protected static function getDb($setFetchClass = false){
        $db = DB::getInstance();
        if ($setFetchClass) {
            $db->setClass(get_called_class());
            $db->setFetchType(PDO::FETCH_CLASS);
        }
        return $db;
    }
    
    
    public static function insert($values){
        $db = static::getDb();
        return $db->insert(static::$table, $values);
    }


    public static function update($values, $conditions){
        $db = static::getDb();
        return $db->update(static::$table, $values, $conditions);
    }


    public function delete(){
        $db = static::getDb();
        $table = static::$table;
        $params = [
            'conditions' => 'id = :id' ,
            'bind' => ['id' => $this->id],
        ];
        list('sql' => $conds, 'bind' => $bind) = self::queryParamBuilder($params);
        $sql = "DELETE FROM {$table} {$conds}";
        return $db->execute($sql, $bind);
    }


    public static function find($params = []){
        $db = static::getDb(true);
        list('sql' => $sql, 'bind' => $bind) = self::selectBuilder($params);
        return $db->query($sql, $bind)->results();
    }

    public static function findFirst($params = []){
        $db = static::getDb(true);
        list('sql' => $sql, 'bind' => $bind) = self::selectBuilder($params);
        $results = $db->query($sql, $bind)->results();
        return isset($results[0]) ? $results[0] : false;
    }

    public static function findById($id){
       return self::findFirst([
         "conditions" => "id = :id",
         "bind" => ["id" => $id]
       ]);
    }


    public static function findTotallyUnique($params = []){
       unset($params['limit']);
       unset($params['offset']);
        $table = static::$table;
        $sql = "SELECT COUNT(*) AS total FROM {$table}";
        list('sql' => $conds, 'bind' => $bind) = self::queryParamBuilder($params);
        $sql .= $conds;
        $db = static::getDb();
        $results = $db->query($sql, $bind);
        $total = $results->getRowCount() > 0 ? $results->results()[0]->total : 0;
        return $total;
    }


    public function save($sql, $bind = null){
        $save = false;
        $this->beforeSave();
        if ($this->__validationPassed) {
            $db = static::getDb();
            $values = $db->getValuesForSave();
            if ($this->isNew()) {
                $save = $db->insert(static::$table, $values);
                
                if ($save) {
                    $this->id = $db->getLastInsertId();
                }

            }else {
                $save = $db->update(static::$table, $values, ['id' => $this->id]);
            }
        }
        return $save;
    }

    public function isNew(){
        return empty($this->id);
    }


    public static function selectBuiler($params = []){
        $columns = array_key_exists('columns', $params) ? $params['columns'] : "";
        $table = static::$table;
        $sql = "SELECT {$columns} FROM {$table}";
        list('sql' => $conds, 'bind' => $bind) = self::queryParamBuilder($params);
        $sql .= $conds;
        return ['sql' => $sql, 'bind' => $bind];
    }

    public static function queryParamBuilder($params = []){
        $sql = "";
        $bind = array_key_exists('bind', $params) ? $params['bind'] : [];

        # Joins conditions
        if (array_key_exists('joins', $params)) {
            $joins = $params['joins'];
            foreach ($joins as $join) {
                $joinTable = $join[0];
                $joinOn = $join[1];
                $joinAlias = isset($join[2]) ? $join[2] : '';
                $joinType = isset($join[3]) ? "{$join[3]} JOIN" : "join";
                $sql .= " {$joinType} {$joinTable} {$joinAlias} ON {$joinOn}";
            }
        }
        
        # Where conditions
        if (array_key_exists('conditions', $params)) {
            $conds = $params['conditions'];
            $sql .= " WHERE {$conds}";
        }

        # Group conditions
        if (array_key_exists('group', $params)) {
            $group = $params['group'];
            $sql .= " GROUP BY {$group}";
        }

        # Order conditions
        if (array_key_exists('order', $params)) {
            $order = $params['order'];
            $sql .= " ORDER BY {$order}";
        }

        # Limit conditions
        if (array_key_exists('limit', $params)) {
           $limit = $params['limit'];
           $sql .= " LIMIT {$limit}";
        }

        # Offset conditions
        if (array_key_exists('offset', $params)) {
            $offset = $params['offset'];
            $sql .= " OFFSET {$offset}";
        }

        return ['sql'=> $sql, 'bind' => $bind];
    }


    public function getValuesForSave(){
        $columns = static::getColumns();
        $values = [];
        foreach ($columns as $column){
            if (!in_array($column, $this->skipUpdate)) {
              $values[$column] = $this->{$column};
            }
        }
        return $values;
    }

    public static function getColumns(){
       if (!static::$columns) {
         $db = static::getDb();
         $table = static::$table;
            $sql = "SHOW COLUMNS FROM {$table}";
            $results = $db->query($sql);
            $columns = [];
            foreach ($results as $column) {
                $columns[] = $column->Field;
            }
            static::$columns = $columns;
       }
       return static::$columns;
    }


    public function runValidation($validator) {
       $validates = $validator->runValidation();
       if (!$validates) {
         $this->__validationPassed = false;
         $this->__errors[$validator->field] = $validates->msg;
       }
    }


    public function getErrors() {
       return $this->__errors;
    }

    public function setError($name, $value) {
      $this->__errors[$name] = $value;
    }

    public function timeStamps($time) {
        $dt = new \DateTime("now", new \DateTimeZone("UTC"));
        $now = $dt->format("Y-m-d H:i:s");
        if ($this->isNew()) {
            $this->created_at = $now;
        }

    }

    public static function mergeWithPagination($params = []) {
        $request = new Request();
        $page = $request->get('p');
        if (!$page || $page < 1){
            $limit = $request->get('limit') ? $request->get('limit') : 25;
            $offset = ($page - 1) * $limit;
            $params['limit'] = $limit;
            $params['offset'] = $offset;
            return $params;
        }
    }

    public function beforeSave() {}

}
?>