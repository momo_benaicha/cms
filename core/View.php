<?php

namespace Core;
use Core\Config;

class View
{
   private $__siteTitle = "";
   private $__content = [];
   private $__concurrentContent;
   private $__buffer;
   private $__layout;
   private $__defaultViewPath;


   public function __construct($path = ""){
      $this->__defaultViewPath = $path;
      $this->__siteTitle = Config::get('default_site_title');
   }

   public function setSiteTitle($title) {
     $this->__siteTitle = $title;
   }

   public function getSiteTitle() {
     return $this->__siteTitle;
   }
   
   public function setLayout($layout) {
      $this->__layout = $layout;
   }

   public function render($path = "") {
        if (empty($path)) {
            $path = $this->__defaultViewPath;
        }
        $layoutPath = PROOT . DS . 'app' . DS . 'views' . DS . 'layouts' . DS . $this->__layout . '.php';
        $fullPath = PROOT . DS . 'app' . DS . 'views' . DS . $path . '.php';
      
        if (!file_exists($fullPath)) {
            throw new \Exception("The view \" {$path} \" does not exist.");
        }
        
        if (!file_exists($layoutPath)) {
          throw new \Exception("The layout \"{$this->__layout}\" does not exist.");
        }
        include($fullPath);
        include($layoutPath);
   }


   public function start($key) {
     if (empty($key)) {
       throw new \Exception("Your start method requires a valid key");
     }
     $this->__buffer = $key;
    ob_start();
   }

   public function end() {
    if (empty($this->__buffer)) {
      throw new \Exception("Your must first call start method");
    }
    $this->__content[$this->__buffer] = ob_get_clean();
    $this->__buffer = null;
   }

   public function content($key) {
     if (array_key_exists($key, $this->__content)) {
      # code...
      echo $this->__content[$key];
     }else{
      echo '';
     }
   }

   public function partial($path) {
      $fullPath = PROOT . DS . 'app' . DS . 'views'  . DS . $path . '.php';
      if (file_exists($fullPath)) {
        include($fullPath);
      }
   }
   

}